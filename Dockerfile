FROM openjdk:8-jre-alpine AS tmp-env
RUN  apk update && apk upgrade && apk add curl

# Downloads and unzips JCE
RUN cd /tmp/ && \
    curl -k -LO "http://download.oracle.com/otn-pub/java/jce/8/jce_policy-8.zip" -H 'Cookie: oraclelicense=accept-securebackup-cookie' && \
    unzip jce_policy-8.zip && \
    rm jce_policy-8.zip

FROM openjdk:8-jre-alpine

# copies JCE from tmp-env
COPY --from=tmp-env /tmp/UnlimitedJCEPolicyJDK8/*.jar /usr/lib/jvm/java-1.8-openjdk/jre/lib/security/

# folders
RUN ["mkdir", "-p", "/opt/app"]
WORKDIR /opt/app

# ports
EXPOSE 8888

# entrypoint
COPY scripts/entrypoint.sh entrypoint.sh
ENTRYPOINT [ "sh", "entrypoint.sh" ]

# app executable
ARG JAR_FILE
ADD target/${JAR_FILE} app.jar

