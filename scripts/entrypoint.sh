#!/bin/sh -e
PROFILE=${1:-"default"}
echo "Starting app..."

java -jar -Dspring.profiles.active=$PROFILE \
          -DENCRYPT_KEY=$ENCRYPT_KEY \
          -Dspring.cloud.config.server.git.username=$CONFIG_SERVER_USERNAME \
          -Dspring.cloud.config.server.git.password=$CONFIG_SERVER_PASSWORD \
          app.jar